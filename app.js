const express = require('express');
let mysql = require('mysql');
let config = require('./config.json');
let Hashids = require('hashids');

let hashids = new Hashids(config.salt);
let app = express();
let pool = mysql.createPool({
    connectionLimit: 10,
    host: config.db_host,
    user: config.db_user,
    password: config.db_password,
    database: config.db_name
});

app.get('/', (request, response) => {
    response.redirect(config.original_page);
});

app.get('/:hash', (request, response) => {
    let hash = request.params.hash;
    let id = hashids.decode(hash)[0];
    if (hash) {
        pool.getConnection(function (err, connection) {
            try {
                connection.beginTransaction(function (err) {
                    let created = new Date();
                    let ip = (request.headers['x-forwarded-for'] ||
                        request.connection.remoteAddress ||
                        request.socket.remoteAddress ||
                        request.connection.socket.remoteAddress).split(",")[0];
                    let referer = request.headers['referer'] || null;
                    let refererQuery = ', referer = ' + (referer ? '\'' + referer + '\'' : 'NULL');
                    console.log('INSERT INTO raw_url_datas SET url_id = ' + id + ', user_agent = \'' + request.get('User-Agent') + '\', ip_address = \'' + ip + '\', created_at = ' + connection.escape(created) + refererQuery);
                    connection.query(
                        'INSERT INTO raw_url_datas SET url_id = ' + id + ', user_agent = \'' + request.get('User-Agent') + '\', ip_address = \'' + ip + '\', created_at = ' + connection.escape(created) + refererQuery
                    );

                    connection.commit(function (err) {
                        if (err) {
                            console.error(err.message);
                            return connection.rollback();
                        }
                    })
                });
            } catch (e) {
                console.error(e.message);
                response.redirect(config.original_page);
            }
        });
        pool.getConnection(function (err, connection) {
            console.log(id);
            connection.query('SELECT address FROM urls WHERE id = ' + id + ' LIMIT 1', function (error, result, fields) {
                if (error) {
                    console.error(error.message);
                    response.redirect(config.original_page);
                }
                response.redirect(result[0].address);
            });
        });
    } else {
        response.redirect(config.original_page);
    }
});

app.listen(3000, '127.0.0.1');